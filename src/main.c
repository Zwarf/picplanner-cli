/* main.c
 *
 * Copyright 2023 Zwarf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include <glib.h>
#include <stdlib.h>

#include "calculations/calculations_sun.h"
#include "calculations/calculations_moon.h"
#include "calculations/calculations_milky_way.h"

#define NUM_DATA_POINTS 1440

struct celestial_data
{
  gboolean outputUgly;

  double north;
  double east;

  gboolean rise;
  gboolean set;

  GDateTime *dateTime;
};

int
start_calculations_sun (struct celestial_data *dataSun)
{
  GDateTime *dateTimeNoon;
  GDateTime *dateTimeRise;
  GDateTime *dateTimeSet;

  int *riseUpperSetLowerIndexSun;

  double    *arrayCoordinatesSun;

  dateTimeNoon = g_date_time_new (g_date_time_get_timezone (dataSun->dateTime),
                                    g_date_time_get_year (dataSun->dateTime),
                                    g_date_time_get_month (dataSun->dateTime),
                                    g_date_time_get_day_of_month (dataSun->dateTime),
                                    12, 0, 0);

  arrayCoordinatesSun = picplanner_get_array_coordinates_sun (dateTimeNoon,
                                                                dataSun->east,
                                                                dataSun->north);

  riseUpperSetLowerIndexSun = picplanner_get_index_rise_upper_set_lower (arrayCoordinatesSun);

  dateTimeRise = g_date_time_add_minutes (dateTimeNoon,
                                          riseUpperSetLowerIndexSun[0]*24*60/NUM_DATA_POINTS-12*60);
  dateTimeSet = g_date_time_add_minutes (dateTimeNoon,
                                         riseUpperSetLowerIndexSun[2]*24*60/NUM_DATA_POINTS-12*60);

  if (!dataSun->outputUgly)
    {
      g_print ("Sun data:\n");
      g_print ("Rise: %02d:%02d  Set: %02d:%02d\n",
               g_date_time_get_hour (dateTimeRise), g_date_time_get_minute (dateTimeRise),
             g_date_time_get_hour (dateTimeSet), g_date_time_get_minute (dateTimeSet));
    }
  else
    {
      g_print ("%02d:%02d %02d:%02d\n",
               g_date_time_get_hour (dateTimeRise), g_date_time_get_minute (dateTimeRise),
               g_date_time_get_hour (dateTimeSet), g_date_time_get_minute (dateTimeSet));
    }

  return EXIT_SUCCESS;
}

gint
main (gint   argc,
      gchar *argv[])
{
  struct celestial_data dataSun;
  struct celestial_data dataMoon;
  struct celestial_data dataMilkyWay;

  g_autoptr(GOptionContext) context = NULL;
  g_autoptr(GError) error = NULL;

  GTimeZone *gTimeZone;
  gboolean version = FALSE;
  gboolean ugly = FALSE;
  gboolean sun = FALSE;
  gboolean moon = FALSE;
  gboolean milkyWay = FALSE;
  gboolean rise = FALSE;
  gboolean set = FALSE;
  gboolean golden = FALSE;
  gboolean blue = FALSE;
  gboolean illumination = FALSE;
  gboolean visibility = FALSE;
  double north = 0.0;
  double east = 0.0;
  double timeZone = 1.0;
  int day = 1;
  int month = 1;
  int year = 2023;
  int hour = 12;
  int minute = 0;
  int list = 0;

  GOptionEntry main_entries[] = {
    { "version", 'v', 0, G_OPTION_ARG_NONE, &version, "Show program version", NULL},
    { "ugly", 'u', 0, G_OPTION_ARG_NONE, &ugly, "Show data only without text", NULL},
    { "north", 'n', 0, G_OPTION_ARG_DOUBLE, &north, "Set the latitude. Northward is positive.", NULL},
    { "east", 'e', 0, G_OPTION_ARG_DOUBLE, &east, "Set the longitude. Eastward is positive.", NULL},
    { "day", 'd', 0, G_OPTION_ARG_INT, &day, "Set the day.", NULL},
    { "month", 'm', 0, G_OPTION_ARG_INT, &month, "Set the month.", NULL},
    { "year", 'y', 0, G_OPTION_ARG_INT, &year, "Set the year.", NULL},
    //{ "hour", 'h', 0, G_OPTION_ARG_INT, &hour, "Set the hour. Only necessary to calculate the current position at this time.", NULL},
    //{ "minute", 'i', 0, G_OPTION_ARG_INT, &minute, "Set the minute. Only necessary to calculate the current position at this time.", NULL},
    { "timezone", 't', 0, G_OPTION_ARG_DOUBLE, &timeZone, "Set the time zone in hours of UTC +x.", NULL},
    { "sun", 's', 0, G_OPTION_ARG_NONE, &sun, "Show calculated data about the sun.", NULL},
    //{ "moon", 'o', 0, G_OPTION_ARG_NONE, &moon, "Show calculated data about the moon.", NULL},
    //{ "milkyway", 'l', 0, G_OPTION_ARG_NONE, &milkyWay, "Show calculated data about the milky way.", NULL},
    { "rise", 'a', 0, G_OPTION_ARG_NONE, &rise, "Show rise data about selected celestials.", NULL},
    { "set", 'u', 0, G_OPTION_ARG_NONE, &set, "Show set data about selected celestials.", NULL},
    //{ "golden", 'g', 0, G_OPTION_ARG_NONE, &golden, "Show additionally to rise/set of the sun the golden hour data.", NULL},
    //{ "blue", 'b', 0, G_OPTION_ARG_NONE, &blue, "Show additionally to rise/set of the sun the blue hour data.", NULL},
    //{ "list", 0, 0, G_OPTION_ARG_INT, &list, "Show the calculated data every specified minute.", NULL},
    //{ "illumination", 0, 0, G_OPTION_ARG_NONE, &illumination, "Show moon illumination data at a specific time.", NULL},
    //{ "visiblility", 0, 0, G_OPTION_ARG_NONE, &visibility, "Show the visibility at a specific day.", NULL},
    { NULL }
  };

  context = g_option_context_new ("- calculate the position of the sun, moon and milky way.");
  g_option_context_add_main_entries (context, main_entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      return EXIT_FAILURE;
    }

  if (version)
    {
      g_printerr ("%s\n", PACKAGE_VERSION);
      return EXIT_SUCCESS;
    }

  if (sun)
    {
      dataSun.north = north;
      dataSun.east = east;
      gTimeZone = g_time_zone_new_offset ((int) timeZone*3600);
      dataSun.dateTime = g_date_time_new (gTimeZone, year, month, day, hour, minute, 0);
      dataSun.rise = rise;
      dataSun.set = set;
      dataSun.outputUgly = ugly;
      start_calculations_sun (&dataSun);
    }

  return EXIT_SUCCESS;
}
